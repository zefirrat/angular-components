import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { BannerComponent, BannerCtrlDirective } from './banner.component';

@NgModule({
    imports:      [ BrowserModule, BrowserAnimationsModule ],
    declarations: [ BannerComponent, BannerCtrlDirective ],
    bootstrap:    [ BannerComponent ]
  })
export class BannerModule { }